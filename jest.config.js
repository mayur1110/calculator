const path = require("path");

module.exports = {
  moduleFileExtensions: ["ts", "tsx", "js"],
  modulePaths: ["src/"],
  setupFiles: [path.resolve(__dirname, "test/helpers/setupEnzyme.js")],
  transform: {
    ".(ts|tsx)": "ts-jest"
  },
  testMatch: ["**/*.test.(js|jsx)"]
};
