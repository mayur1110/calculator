# Calculator React APP

# DEVELOPMENT:

## RUNNING THE APPLICATION
- `npm run serve:dev` builds the application and serves it with webpack
- Open your browser and go to `http://localhost:8080/`.
- Now any changes in client code will reflect to browser automatically on reload.

```
npm run serve:dev

http://localhost:8080/
```

## UNIT TEST APPLICATION

```
npm run test
```