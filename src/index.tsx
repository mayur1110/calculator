import * as React from "react";
import * as ReactDOM from "react-dom";
import App from "app";

// render appication on DOM
ReactDOM.render(<App />, document.getElementById("app"));
