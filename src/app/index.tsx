import * as React from "react";
import Calculator from "app/components/calculator";
import Block from "app/components/block";

// component namespaces and interface
namespace App {
  export interface State {
    num1: number;
    num2: number;
    num3: number;
  }
}

//////////////////////////////

/**
 * App class respresenting main component
 *
 * @class
 */
export default class App extends React.Component<{}, App.State> {
  constructor(props) {
    super(props);

    this.state = {
      num1: 0,
      num2: 0,
      num3: 0
    };
  }

  handleChange = event => {
    let { name, value } = event.target;

    this.setState(({
      [name]: Number(value)
    } as unknown) as App.State);
  };

  /**
   * render DOM on state updates
   */
  render() {
    const { num1, num2, num3 } = this.state;

    return (
      <div style={{ textAlign: "center" }}>
        <Block color="deepskyblue">
          Value 1:
          <input
            type="text"
            name="num1"
            value={num1}
            onChange={this.handleChange}
          />
        </Block>
        <Block color="deepskyblue">
          Value 2:
          <input
            type="text"
            name="num2"
            value={num2}
            onChange={this.handleChange}
          />
        </Block>
        <Block color="deepskyblue">
          Value 3:
          <input
            type="text"
            name="num3"
            value={num3}
            onChange={this.handleChange}
          />
        </Block>
        <Block color="lightpink">
          <Calculator numbers={[num1, num2, num3]} />
        </Block>
      </div>
    );
  }
}
