import * as React from "react";
import * as styles from "./styles.css";

// component namespaces and interface
namespace Block {
  export interface Props {
    color: string; // block color
  }
}

//////////////////////////////

/**
 * Block class component
 *
 * @class
 */
export default class Block extends React.Component<Block.Props, {}> {
  /**
   * render DOM on state updates
   */
  render() {
    return (
      <div
        className={styles.outerBlock}
        style={{ backgroundColor: this.props.color }}
      >
        <div className={styles.innerBlock}>{this.props.children}</div>
      </div>
    );
  }
}
