import Calculator from ".";
import * as React from "react";
import { shallow, mount } from "enzyme";

describe("Calculator", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Calculator numbers={[2, 3, 4]} />);
  });

  test("should exist and setup initial state", () => {
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.state("operation")).toEqual("add");
  });

  test("should have radio buttons to select 'add' or 'multiply' option", () => {
    expect(wrapper.find("input[type='radio'][value='add']")).toHaveLength(1);
    expect(wrapper.find("input[type='radio'][value='multiply']")).toHaveLength(
      1
    );
  });

  test("should have 'add' operation by default and calculate function add numbers", () => {
    const instance = wrapper.instance();

    expect(instance.calculate([2, 3, 4])).toEqual(9);
  });

  test("should select 'add' operation and calculate function add numbers", () => {
    const instance = wrapper.instance();
    wrapper.setState({
      operation: "add"
    });

    expect(instance.calculate([2, 3, 4])).toEqual(9);
  });

  test("should select 'multiply' operation and calculate function multiply numbers", () => {
    const instance = wrapper.instance();
    wrapper.setState({
      operation: "multiply"
    });

    expect(instance.calculate([2, 3, 4])).toEqual(24);
  });
});
