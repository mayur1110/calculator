import * as React from "react";
import { OPERATION } from "app/constants";

// component namespaces and interface
namespace Calculator {
  export interface Props {
    numbers: number[];
  }
  export interface State {
    operation: "add" | "multiply";
  }
}

//////////////////////////////

/**
 * Calculator class component
 *
 * @class
 */
export default class Calculator extends React.Component<
  Calculator.Props,
  Calculator.State
> {
  constructor(props: Calculator.Props) {
    super(props);

    // set "add" as a default operation.
    this.state = {
      operation: OPERATION.ADD as "add"
    };
  }

  /**
   * Calculate a result for a given set of numbers.
   *
   * @param {number[]} nums set of numbers
   *
   * @returns {number} result.
   */
  calculate = (nums: number[]) => {
    return nums.reduce((result, num) => this.doOperate(result, num));
  };

  /**
   * Does operation on given two numbers.
   *
   * @param {number} num1 first number.
   * @param {number} num2 second number.
   *
   * @returns {number} result.
   */
  doOperate = (num1: number, num2: number) => {
    const operator = this.state.operation;

    switch (operator) {
      case OPERATION.ADD:
        return num1 + num2;
      case OPERATION.MULTIPLY:
        return num1 * num2;
      default:
        return null;
    }
  };

  /**
   * Update selected operation in state
   */
  handleOperatorChange = event => {
    this.setState({
      operation: event.target.value
    });
  };

  /**
   * render DOM on state updates
   */
  render() {
    const { numbers } = this.props;
    const { operation } = this.state;

    return (
      <React.Fragment>
        <div>
          <input
            type="radio"
            value="add"
            checked={operation === OPERATION.ADD}
            onChange={this.handleOperatorChange}
          />
          Sum
        </div>
        <div>
          <input
            type="radio"
            value="multiply"
            checked={operation === OPERATION.MULTIPLY}
            onChange={this.handleOperatorChange}
          />
          Multiply
        </div>

        <p> Result: {numbers && this.calculate(numbers)} </p>
      </React.Fragment>
    );
  }
}
